/*
  Name: list.cpp--implementing the member functions for our array-based list ADT
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:43
  Description: Implementation of our array-based list ADT
*/

#include <iostream>
#include "list.h"  // header file

List::List(): size(0)
{
   //Initialize items array to 0
   for (int i = 0 ; i < MAX_LIST; i++)
	   items[i] = 0;

}  // end default constructor

bool List::isEmpty() const
{
   return size == 0;
}  // end isEmpty

int List::getLength() const
{
   return size;
}  // end getLength


//Returns the index to insert dataItem
int List::getInsertPoint( const ListItemType & dataItem ) const
{
	int index = 0;
	for (index = 0; index < size && items[index] <= dataItem; index++)
		;

	//Return the index
	return index;
}


void List::insert( const ListItemType& newItem ) throw(ListException)
{
	int index = 0;
	int newSize = 0;
	//First find the index of our itemToRemove
	index = getInsertPoint(newItem);

	if (size >= MAX_LIST)
	{
		//Our list is full, throw ListException, we can't insert into it
		throw ListException("List full, can't complete insert operation!");
	}
	else 
	{
		for (int i =  size; i > index; i--)
		{
			swapItems(i, i-1);
		}
		//Now insert out item
		items[index] = newItem;

		//Increment size
		size++;
	}
}

void List::remove( const ListItemType & itemToRemove ) throw(ListException)
{
	int index = 0;
	int newSize = 0;
	//First find the index of our itemToRemove

	index = binSearch(itemToRemove, 0, size-1);

	if (index != -1)
	{
		//We found the item so lets first remove it
		items[index] = 0;

		newSize = (size) - (index + 1);
		for (int i = 0; i < newSize; i++)
		{
			swapItems((index+i), (index+1+i));
		}

		//Decrement size
		size--;
	}
	else
	{
		//Can't remove item, throw ListException
		throw ListException("Item not found, can't remove item from list!");
	}
}

void List::print() const
{


	//Print out the list
	
	cout << "[ ";
	for (int i = 0; i < size; i ++)
	{
		cout << items[i] << " ";
	}

	cout << " ]";
	cout << "\n";
}

int List::binSearch( const ListItemType & dataItem, int min, int max ) const
{
	if (max < min)
			return -1;
	else
	{
		// calculate midpoint to cut set in half
		int mid = midpoint(min, max);

		// item is either in lower interval, higher interval, or a match
		if (items[mid] > dataItem)
			// item is in lower interval
				return binSearch(dataItem, min, mid-1);
		else if (items[mid] < dataItem)
			// item is in upper interval
			return binSearch(dataItem, mid+1, max);
		else
			// key has been found
			return mid;
	}
}

int List::midpoint( int min, int max ) const
{
	float val = 0;
	val = max-min;
	val = val / 2;
	val = ceil(val);
	val = val + min; //val is independent, we need to stick it in our interval now
	//Return max-min / 2 rounded up
	return  (int) val;
	
	
}

void List::swapItems( int index1, int index2 )
{
	//Helper function to swap to elements of an array
	int swap;
	swap = items[index1];
	items[index1] = items[index2];
	items[index2] = swap;
}



