/*
  Name: list.h--consisting of the interface of List
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:42
  Description: A basic list object using array-based implementation that can be used with indexes as a vector. Or using insert clauses 
  without specifying indexes to have the list maintain elements in order.
*/

/** @file list.h */
#ifndef _LIST_H
#define _LIST_H

#include "listExceptions.h"

const int MAX_LIST = 20;
typedef int ListItemType;

/** @class List
 * ADT list - Array-based implementation with exceptions */
class List
{
public:
   List();
   // destructor is supplied by compiler

   /** @throw None. */
   bool isEmpty() const;

   /** @throw None. */
   int getLength() const;


   //New function added -Frank
    /**
    *  @throw ListException  If newItem cannot be placed in the list
    *         because the array is full. */
   void insert(const ListItemType& newItem)
	   throw(ListException); 
   
   //My Function-Frank
   /** @throw ListException thrown if we can't locate the  item to remove  
    *          */
   void remove(const ListItemType & itemToRemove)
	   throw(ListException);

   //My function to print the contents of the list 
   void print() const;

  
private:
   /** array of list items */
   ListItemType items[MAX_LIST];

   /** number of items in list */
   int          size;

    /** returns index in array where dataItem is found
		returns -1 if dataItem not found
	*/
   int binSearch( const ListItemType & dataItem, int min, int max ) const;
   //Swap items[index1] for items[index2]
   void swapItems(int index1, int index2);

   //Get insert point, returns the index to insert dataItem at.
   int getInsertPoint(const ListItemType & dataItem) const;

   //Helper function to find the midpoint of a range
   int midpoint(int min, int max) const;

}; // end List
// End of header file.

#endif
