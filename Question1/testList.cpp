/*
  Name: testList.cpp
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:49
  Description: A driver class to test out our list class as an in order list and a vector with operator overloading.
*/

#include <iostream>
#include "list.h" // ADT list operations

using namespace std;

int main()
{
   List			inOrderList;
   List         aList;
   List			bList;
   ListItemType dataItem;


   try 
   {
	   //Test Question 1
	   cout << "Question 1, an inorder list: " << endl;
	   inOrderList.insert(1);
	   inOrderList.insert(20);
	   inOrderList.insert(35);
	   inOrderList.insert(3);
	   inOrderList.insert(7);
	   inOrderList.print();

	   cout << "Now remove 20 and 35" << endl;
	   inOrderList.remove(20);
	   inOrderList.remove(35);
	   inOrderList.print();

	   cout << "Remove the rest of the entries" << endl;
	   inOrderList.remove(1);
	   inOrderList.remove(3);
	   inOrderList.remove(7);
	   inOrderList.print();

	   cout<< "Fill up our vector to its max" << endl;
	   for (int i = 0 ; i < MAX_LIST; i++)
		   inOrderList.insert(i);

	   inOrderList.print();

	   cout << "Remove 1-3" << endl;
	   inOrderList.remove(1);
	   inOrderList.remove(2);
	   inOrderList.remove(3);
	   inOrderList.print();


	   
   }
   catch( ListException & e1)
   {
          cerr << e1.what();
   }
   catch ( ListIndexOutOfRangeException & e2)
   {
       //handling the exception here
	   cerr << e2.what();
   } 
   catch (...)
   {
         //handling all the other exceptions here
	   cerr << "Some other shit happened!";
   }
   
   
   return 0;

}
