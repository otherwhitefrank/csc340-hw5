/*
  Name: NewVectorExceptions.h
  Desc: declare two exception classes that are specific to NewVector
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:37
  Description: 
*/
#ifndef _NewVector_EXCEPTIONS_H
#define _NewVector_EXCEPTIONS_H

#include <stdexcept>
#include <string>

using namespace std;

class NewVectorIndexOutOfRangeException : public out_of_range
{
public:
   
   /**/
   NewVectorIndexOutOfRangeException(const string & message = "")
      : out_of_range(message.c_str())
   { }  // end constructor
   /**/
   //virtual const char* what() const throw()
   //{return "NewVector: out of range";}
}; // end NewVectorIndexOutOfRangeException


class NewVectorException : public logic_error
{
public:
   
   /**/    
   NewVectorException(const string & message = "")
      : logic_error(message.c_str())
   { }  // end constructor
   /**/
   // virtual const char* what() const throw()
   //{return "NewVector: logic error";}   //NewVector is full
}; // end NewVectorException

#endif
