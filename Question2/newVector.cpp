/*
  Name: NewVector.cpp--implementing the member functions for our array-based NewVector ADT
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:43
  Description: Implementation of our array-based NewVector ADT
*/

#include <iostream>
#include "NewVector.h"  // header file

NewVector::NewVector(): size(0)
{
   //Initialize items array to 0
   for (int i = 0 ; i < MAX_NewVector; i++)
	   items[i] = 0;

}  // end default constructor

bool NewVector::isEmpty() const
{
   return size == 0;
}  // end isEmpty

int NewVector::getLength() const
{
   return size;
}  // end getLength


//Index starts at 0, using translate was silly.
void NewVector::insert(int index, const NewVectorItemType& newItem)
	throw(NewVectorIndexOutOfRangeException, NewVectorException)
{
	//Rewrote this function to just use 0 as the starting index, using translate function is silly
	//-Frank

	NewVectorException NewVectorEx("NewVectorException: NewVector full on insert");
	NewVectorIndexOutOfRangeException outOfRange("NewVectorIndexOutOfRangeException: Bad index on insert");     

	if (size >= MAX_NewVector)
		throw NewVectorEx;

	if (index <= size && index >= 0)
	{
		for (int pos = size; pos >= index; --pos)
			items[pos+1] = items[pos];
		// insert new item
		items[index] = newItem;
		++size;  // increase the size of the NewVector by one
	}
	else  // index out of range
		throw outOfRange;
	
	// end if
}  // end insert




void NewVector::print() const
{
	//Print out the NewVector
	
	cout << "[ ";
	for (int i = 0; i < size; i ++)
	{
		cout << items[i] << " ";
	}

	cout << "]";
	cout << "\n";
}



NewVector NewVector::operator++( int )
{
	NewVector tempNewVector;

	//Create NewVector that is the same to return
	for (int i = 0; i < size; i++)
		tempNewVector.insert(i, items[i]);

	for (int i = 0; i < size; i++)
		items[i]++;

	//Return the original NewVector, then increment each item in the NewVector afterwards
	return tempNewVector;
}

NewVector NewVector::operator++()
{
	NewVector tempNewVector;

	//Prefix, increment the NewVector and return it
	for (int i = 0; i < size; i++)
		tempNewVector.insert(i, items[i] + 1);

	for (int i = 0; i < size; i++)
		items[i]++;

	return tempNewVector;
}

ostream &operator<<( ostream &out, const NewVector &L ) {
	out << "[ ";
	for (int i=0; i < L.getLength(); i++) 
	{
		out << L.newRetrieve(i) << ' ';
	}
	out << ']';
	return out;
}

NewVector NewVector::operator/( float f1 )
{
	int length = this->getLength();
	NewVector tempNewVector;

	NewVectorException NewVectorEx("NewVectorException: Division by Zero on operator/");
	 

	//First check lengths match
	if (f1 != 0)
	{
		//Continue, same length
		for (int i = 0; i < length; i++)
		{
			float a = 0;
			a = this->newRetrieve(i);
			tempNewVector.insert(i,a / f1);
		}

	}
	else
		throw NewVectorEx;

	return tempNewVector;
}

NewVector NewVector::operator*( float f1 )
{
	int length = 0;
	
	length = this->getLength();
	NewVector tempNewVector;

	//Continue, same length
	for (int i = 0; i < length; i++)
	{
		tempNewVector.insert(i,this->newRetrieve(i) * f1);
	}
	
	return tempNewVector;

}

NewVector NewVector::operator-( NewVector A)
{
	int aLength = 0;
	int bLength = 0;
	aLength = A.getLength();
	bLength = this->getLength();
	NewVector tempNewVector;

	NewVectorException NewVectorEx("NewVectorException:Two NewVector's lengths do not match, cannot perform - operator");


	//First check lengths match
	if (aLength == bLength)
	{
		//Continue, same length
		for (int i = 0; i < aLength; i++)
		{
			tempNewVector.insert(i,A.newRetrieve(i) - this->newRetrieve(i));
		}

	}
	else
		throw NewVectorEx;

	return tempNewVector;
}

NewVector NewVector::operator+( NewVector A)
{
	int aLength = 0;
	int bLength = 0;
	aLength = A.getLength();
	bLength = this->getLength();
	NewVector tempNewVector;

	NewVectorException NewVectorEx("NewVectorException:Two NewVector's lengths do not match, cannot perform + operator");


	//First check lengths match
	if (aLength == bLength)
	{
		//Continue, same length
		for (int i = 0; i < aLength; i++)
		{
			tempNewVector.insert(i,A.newRetrieve(i) + this->newRetrieve(i));
		}

	}
	else
		throw NewVectorEx;
	
	return tempNewVector;
}

NewVectorItemType NewVector::newRetrieve( int index ) const throw(NewVectorIndexOutOfRangeException)
{
	bool success;
	NewVectorIndexOutOfRangeException outOfRange("NewVectorIndexOutOfRangeException: Tried to access index out of range in newRetrieve");

	success = (index <= size) && (index >= 0);

	if (!success) throw outOfRange;

	if (success)
		return items[index];

}
