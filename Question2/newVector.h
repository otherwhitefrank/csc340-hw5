/*
  Name: NewVector.h--consisting of the interface of NewVector
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:42
  Description: A basic NewVector object using array-based implementation that can be used with indexes as a vector. 
*/

/** @file NewVector.h */
#ifndef _NewVector_H
#define _NewVector_H

#include "NewVectorExceptions.h"

const int MAX_NewVector = 20;
typedef float NewVectorItemType;

/** @class NewVector
 * ADT NewVector - Array-based implementation with exceptions */
class NewVector
{
public:
   NewVector();
   // destructor is supplied by compiler

   /** @throw None. */
   bool isEmpty() const;

   /** @throw None. */
   int getLength() const;

   /** @throw NewVectorIndexOutOfRangeException  If index < 1 or index >
    *         getLength() + 1.
    *  @throw NewVectorException  If newItem cannot be placed in the NewVector
    *         because the array is full. */
   void insert(int index, const NewVectorItemType& newItem)
      throw(NewVectorIndexOutOfRangeException, NewVectorException);
   
   //The other version of retrieve is excessive, this just returns a value when asked, very simple.
   NewVectorItemType newRetrieve(int index) const
	   throw(NewVectorIndexOutOfRangeException);

   //My function to print the contents of the NewVector 
   void print() const;

   //Overloaded Operators
   //overload the + operator as a member function to add two vectors
	NewVector operator + (NewVector);

	//overload the - operator as a member function to subtract one vector from another
	NewVector operator - (NewVector);

	//overload the * operator as a member function to multiply a vector by a real number
	NewVector operator * (float f1);

	//overload the / as a member function to divide a vector by a real number
	NewVector operator / (float f1);
		
	//overload the prefix and postfix ++ operators as two member functions to increment each element in a vector by 1
	//This is prefix
	NewVector operator ++ ();

	//overload the prefix and postfix ++ operators as two member functions to increment each element in a vector by 1
	//This is postfix
	NewVector operator ++ (int) ;

	//overload the << operator to print out a vector 
	friend ostream &operator<<( ostream &out, const NewVector &L );

private:
   /** array of NewVector items */
   NewVectorItemType items[MAX_NewVector];

   /** number of items in NewVector */
   int          size;

}; // end NewVector
// End of header file.

#endif
