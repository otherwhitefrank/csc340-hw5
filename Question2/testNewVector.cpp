/*
  Name: testNewVector.cpp
  Copyright: Frank Dye, 10/15/2013
  Author: Frank Dye, ID: 912927332
  Date: 19/09/11 18:49
  Description: A driver class to test out our NewVector class as an in order NewVector and a vector with operator overloading.
*/

#include <iostream>
#include "NewVector.h" // ADT NewVector operations

using namespace std;


int main()
{
   NewVector			inOrderNewVector;
   NewVector         aNewVector;
   NewVector			bNewVector;
   NewVectorItemType dataItem;

   try 
   {
	   NewVector aVector, bVector;
	   //Test Question 2, treat aList and bList as vectors
	   cout << "Question 2: two vectors a and b" << endl;
	   aVector.insert(0, 3);
	   aVector.insert(1, 2);
	   aVector.insert(2, 6);
	   bVector.insert(0, 3);
	   bVector.insert(1, 2);
	   bVector.insert(2, 6);
	   cout << "a: " << aVector << endl;
	   cout << "b: " << bVector << endl;
	   cout << "a+b: " << aVector + bVector << endl;
	   cout << "a-b: " << aVector - bVector << endl;
	   cout << "a*4: " << aVector * 4.0 << endl;
	   cout << "a/4: " << aVector / 4.0 << endl;
	   cout << "++a: " << ++aVector << endl;
	   cout << "a: " << aVector << endl;
	   cout << "b++: " << bVector++ << endl;
	   cout << "b: " << bVector;

   }
   catch( NewVectorException & e1)
   {
	   //handling NewVectorException, e1.what() will returns whatever specific message and output it to standard error
       cerr << e1.what();
   }
   catch ( NewVectorIndexOutOfRangeException & e2)
   {
       //handling IndexOutOfRangeException
	   cerr << e2.what();
   } 
   catch (...)
   {
       //handling all the other exceptions here
	   cerr << "Additional Exceptions happened";
   }
   return 0;

}
